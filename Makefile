ifdef PROJECT
.PHONY: new-bin new-lib
new-bin new-lib: new-%:
	alr init --$* $(PROJECT)

.PHONY: add-tests
add-tests:
	alr init --bin tests
	cd tests && \
		alr with --use=.. $(PROJECT)
endif
