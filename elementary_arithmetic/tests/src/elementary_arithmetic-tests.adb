with Trendy_Test.Assertions; use Trendy_Test.Assertions;

package body Elementary_Arithmetic.Tests is

   procedure Test_To_Number_01 (T : in out Trendy_Test.Operation'Class) is
      S : constant String := "123.456_789";
      N : constant Number := To_Number (S);
   begin
      T.Register;
      Assert (T, N = (N_Digits => 9,
                      Radix => 7,
                      MSD => 9,
                      Digit_List => (9, 8, 7, 6, 5, 4, 3, 2, 1)));
   end Test_To_Number_01;

   procedure Test_To_Number_02 (T : in out Trendy_Test.Operation'Class) is
      S : constant String := "000.000_001";
      N : constant Number := To_Number (S);
   begin
      T.Register;
      Assert (T, N = (N_Digits => 7,
                      Radix => 7,
                      MSD => 7,
                      Digit_List => (1, 0, 0, 0, 0, 0, 0)));
   end Test_To_Number_02;

   function All_Tests return Trendy_Test.Test_Group is
   begin
      return
         (
          Test_To_Number_01'Access,
          Test_To_Number_02'Access
         );
   end All_Tests;

end Elementary_Arithmetic.Tests;
