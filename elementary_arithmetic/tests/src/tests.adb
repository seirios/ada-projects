with Trendy_Test.Reports;
with Elementary_Arithmetic.Tests;
with Elementary_Arithmetic.Addition.Tests;

procedure Tests is
begin
   Trendy_Test.Register (Elementary_Arithmetic.Tests.All_Tests);
   Trendy_Test.Register (Elementary_Arithmetic.Addition.Tests.All_Tests);
   Trendy_Test.Reports.Print_Basic_Report (Trendy_Test.Run);
end Tests;
