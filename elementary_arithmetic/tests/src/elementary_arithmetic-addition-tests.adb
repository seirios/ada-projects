with Trendy_Test.Assertions; use Trendy_Test.Assertions;

package body Elementary_Arithmetic.Addition.Tests is

   procedure Test_Add_01 (T : in out Trendy_Test.Operation'Class) is
      S1 : constant String := "0.99";
      S2 : constant String := "9.09";
      N1 : constant Number := To_Number (S1);
      N2 : constant Number := To_Number (S2);
      N  : constant Number := Add (N1, N2);
   begin
      T.Register;
      Assert (T, N = (N_Digits => 4,
                      Radix => 3,
                      MSD => 4,
                      Digit_List => (8, 0, 0, 1)));
   end Test_Add_01;

   procedure Test_Add_02 (T : in out Trendy_Test.Operation'Class) is
      S1 : constant String := "125408559384699.000007623400";
      S2 : constant String := "000.39435890123";
      N1 : constant Number := To_Number (S1);
      N2 : constant Number := To_Number (S2);
      N  : constant Number := Add (N1, N2);
   begin
      T.Register;
      Assert (T, N = (N_Digits => 26,
                      Radix => 12,
                      MSD => 26,
                      Digit_List => (3, 6, 4, 2, 5, 6, 6, 3, 4, 9, 3,
                                     9, 9, 6, 4, 8, 3, 9, 5, 5, 8, 0,
                                     4, 5, 2, 1)));
   end Test_Add_02;

   procedure Test_Add_03 (T : in out Trendy_Test.Operation'Class) is
      S1 : constant String := "99";
      S2 : constant String := "9";
      N1 : constant Number := To_Number (S1);
      N2 : constant Number := To_Number (S2);
      N  : constant Number := Add (N1, N2);
   begin
      T.Register;
      Assert (T, N = (N_Digits => 3,
                      Radix => 1,
                      MSD => 3,
                      Digit_List => (8, 0, 1)));
   end Test_Add_03;

   function All_Tests return Trendy_Test.Test_Group is
   begin
      return
         (
          Test_Add_01'Access,
          Test_Add_02'Access,
          Test_Add_03'Access
         );
   end All_Tests;

end Elementary_Arithmetic.Addition.Tests;
