with Ada.Text_IO;
with Check_Numeric_String;

package body Elementary_Arithmetic is

   --  Check numeric string
   function Parse_Numeric_String (S : String;
      First : out Natural;
      Last  : out Natural)
      return Natural
   is
      Accepted : Boolean;
      Count : Natural;
      Radix_Position : Integer;
      Last_Leading_Zero : Integer;
      First_Trailing_Zero : Integer;
   begin
      Accepted := Check_Numeric_String (S, Count, Radix_Position,
                                        Last_Leading_Zero,
                                        First_Trailing_Zero);
      if not Accepted then
         raise Bad_Numeric_String with "Invalid numeric string";
      end if;

      --  Trim start
      First := S'First;
      if Last_Leading_Zero in S'Range then
         if Last_Leading_Zero = S'Last
            or else Last_Leading_Zero + 1 = Radix_Position
         then --  keep leading zero 0.SOMETHING
            First := Last_Leading_Zero;
         else --  discard leading zero 0SOMETHING
            First := Last_Leading_Zero + 1;
         end if;
      end if;

      --  Trim end
      Last := S'Last;
      if First_Trailing_Zero in S'Range then
         if First_Trailing_Zero - 1 = Radix_Position
         then --  discard radix point SOMETHING.0
            Last := First_Trailing_Zero - 2;
         else --  discard trailing zero SOMETHING0
            Last := First_Trailing_Zero - 1;
         end if;
      end if;

      Count := 0;
      for C of S (First .. Last) loop
         case C is
            when '0' .. '9' => Count := Count + 1;
            when others => null;
         end case;
      end loop;

      return Count;
   end Parse_Numeric_String;

   --  Parse string representing a number
   function To_Number (S : String) return Number is
      First : Natural;
      Last  : Natural;
      N_Digits : constant Positive :=
         Parse_Numeric_String (S, First, Last);
      Digit_List : Digit_Array (1 .. N_Digits) := (others => 0);
      Radix : Positive := 1;
      IX : Natural := 0;
   begin
      --  Store digits (little-endian)
      --  and location of radix point
      for C of reverse S (First .. Last) loop
         case C is
            when '0' .. '9' =>
               IX := IX + 1;
               Digit_List (IX) := Digit'Value ((1 => C));
            when '.' =>
               Radix := IX + 1;
            when others => null; --  Irrelevant
         end case;
      end loop;
      --  Construct record
      return (N_Digits => N_Digits,
              MSD => N_Digits,
              Radix => Radix,
              Digit_List => Digit_List);
   end To_Number;

   function Compute_MSD (Digit_List : Digit_Array; Radix : Positive)
      return Positive is
   begin
      for I in reverse 1 .. Digit_List'Last loop
         if Digit_List (I) /= 0
            or else (Digit_List (I) = 0 and then I = Radix)
         then
            return I;
         end if;
      end loop;
      return 1;
   end Compute_MSD;

   procedure Print (N : Number) is
      use Ada.Text_IO;
      package IntIO is new Integer_IO (Digit);
   begin
      for I in reverse N.Digit_List'First .. N.MSD loop
         IntIO.Put (N.Digit_List (I), Width => 1);
         if I = N.Radix and then I /= N.Digit_List'First then
            Put (".");
         end if;
      end loop;
      New_Line;
   end Print;

end Elementary_Arithmetic;
