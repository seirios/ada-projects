package Elementary_Arithmetic is

   type Number (N_Digits : Positive) is private;

   Bad_Numeric_String : exception;
   function To_Number (S : String) return Number;

   procedure Print (N : Number);

private

   subtype Digit is Integer range 0 .. 9;
   type Digit_Array is array (Positive range <>) of Digit;

   type Number (N_Digits : Positive) is record
      --  location of most significant digit
      MSD : Positive := 1;
      --  location of decimal point (after this digit)
      Radix : Positive := 1;
      --  list of digits (little-endian)
      Digit_List : Digit_Array (1 .. N_Digits) := (others => 0);
   end record
      with Type_Invariant => (
        Number.MSD <= Number.N_Digits
        and then Number.Radix <= Number.MSD
        and then (Number.Digit_List (1) /= 0
                  or else Number.N_Digits = 1
                  or else Number.Radix = 2)
        and then (Number.Digit_List (Number.N_Digits) /= 0
                  or else Number.N_Digits = 1));

   function Compute_MSD (Digit_List : Digit_Array;
                         Radix : Positive) return Positive;

end Elementary_Arithmetic;
