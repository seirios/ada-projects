--  Generated with FSM_Generator
function Check_Numeric_String (Input : String;
   Count : out Natural;
   Radix_Position : out Integer;
   Last_Leading_Zero : out Integer;
   First_Trailing_Zero : out Integer) return Boolean
is
   type State is (START, SIGN, LEADING_ZERO,
      PRE_RADIX_DIGIT, RADIX_POINT, POST_RADIX_DIGIT,
      TRAILING_ZERO, LEADING_ZERO_SEPARATOR, PRE_RADIX_DIGIT_SEPARATOR,
      TRAILING_ZERO_SEPARATOR, POST_RADIX_DIGIT_SEPARATOR);
   S : State := START;
   S_Prev : State;
begin
   --  Handle initialization of extra parameters
   Count := 0;
   Radix_Position := -1;
   Last_Leading_Zero := -1;
   First_Trailing_Zero := -1;
   --  Iterate over input
   for I in Input'Range loop
      S_Prev := S;
      case S is
      when START =>
         case Input (I) is
         when '+' | '-' =>
            S := SIGN;
         when '0' =>
            S := LEADING_ZERO;
         when '1' .. '9' =>
            S := PRE_RADIX_DIGIT;
         when others =>
            return False;
         end case;
      when SIGN =>
         case Input (I) is
         when '0' =>
            S := LEADING_ZERO;
         when '1' .. '9' =>
            S := PRE_RADIX_DIGIT;
         when others =>
            return False;
         end case;
      when LEADING_ZERO =>
         case Input (I) is
         when '.' =>
            S := RADIX_POINT;
         when '0' =>
            S := LEADING_ZERO;
         when '1' .. '9' =>
            S := PRE_RADIX_DIGIT;
         when '_' =>
            S := LEADING_ZERO_SEPARATOR;
         when others =>
            return False;
         end case;
      when PRE_RADIX_DIGIT =>
         case Input (I) is
         when '.' =>
            S := RADIX_POINT;
         when '0' .. '9' =>
            S := PRE_RADIX_DIGIT;
         when '_' =>
            S := PRE_RADIX_DIGIT_SEPARATOR;
         when others =>
            return False;
         end case;
      when RADIX_POINT =>
         case Input (I) is
         when '0' =>
            S := TRAILING_ZERO;
         when '1' .. '9' =>
            S := POST_RADIX_DIGIT;
         when others =>
            return False;
         end case;
      when POST_RADIX_DIGIT =>
         case Input (I) is
         when '0' =>
            S := TRAILING_ZERO;
         when '1' .. '9' =>
            S := POST_RADIX_DIGIT;
         when '_' =>
            S := POST_RADIX_DIGIT_SEPARATOR;
         when others =>
            return False;
         end case;
      when TRAILING_ZERO =>
         case Input (I) is
         when '0' =>
            S := TRAILING_ZERO;
         when '1' .. '9' =>
            S := POST_RADIX_DIGIT;
         when '_' =>
            S := TRAILING_ZERO_SEPARATOR;
         when others =>
            return False;
         end case;
      when LEADING_ZERO_SEPARATOR =>
         case Input (I) is
         when '0' =>
            S := LEADING_ZERO;
         when '1' .. '9' =>
            S := PRE_RADIX_DIGIT;
         when others =>
            return False;
         end case;
      when PRE_RADIX_DIGIT_SEPARATOR =>
         case Input (I) is
         when '0' =>
            S := PRE_RADIX_DIGIT;
         when '1' .. '9' =>
            S := PRE_RADIX_DIGIT;
         when others =>
            return False;
         end case;
      when TRAILING_ZERO_SEPARATOR =>
         case Input (I) is
         when '0' =>
            S := TRAILING_ZERO;
         when '1' .. '9' =>
            S := POST_RADIX_DIGIT;
         when others =>
            return False;
         end case;
      when POST_RADIX_DIGIT_SEPARATOR =>
         case Input (I) is
         when '0' =>
            S := POST_RADIX_DIGIT;
         when '1' .. '9' =>
            S := POST_RADIX_DIGIT;
         when others =>
            return False;
         end case;
      end case;
      --  Handle state entry actions
      case S is
      when LEADING_ZERO =>
         Count := Count + 1;
         Last_Leading_Zero := I;
      when PRE_RADIX_DIGIT =>
         Count := Count + 1;
      when RADIX_POINT =>
         if S /= S_Prev then
            if Radix_Position in Input'Range then
               return False;
            end if;
            Radix_Position := I;
         end if;
      when POST_RADIX_DIGIT =>
         Count := Count + 1;
         if S /= S_Prev then
            First_Trailing_Zero := -1;
         end if;
      when TRAILING_ZERO =>
         Count := Count + 1;
         if S /= S_Prev then
            if First_Trailing_Zero not in Input'Range then
               First_Trailing_Zero := I;
            end if;
         end if;
      when others => null;
      end case;
   end loop;
   --  Handle EOF
   case S is
   when LEADING_ZERO =>
      return True;
   when PRE_RADIX_DIGIT =>
      return True;
   when POST_RADIX_DIGIT =>
      return True;
   when TRAILING_ZERO =>
      return True;
   when others =>
      return False;
   end case;
end Check_Numeric_String;
