package body Elementary_Arithmetic.Addition is

   function Add (N1 : Number; N2 : Number) return Number is
      Max_Pre_Digits : constant Integer :=
         Integer'Max (N1.MSD - N1.Radix + 1,
                      N2.MSD - N2.Radix + 1);
      Max_Post_Digits : constant Integer :=
         Integer'Max (N1.Radix - 1, N2.Radix - 1);
      Radix : constant Positive := Positive'Max (N1.Radix, N2.Radix);
      N_Digits : Positive := Max_Pre_Digits + Max_Post_Digits + 1;
      Digit_List : Digit_Array (1 .. N_Digits) := (others => 0);
      Carry : Integer range 0 .. 1 := 0;
      D_Sum : Natural;
      A1 : Digit;
      A1_Index : Integer;
      A2 : Digit;
      A2_Index : Integer;
   begin
      --  Iterate over digits (zero if out of bounds)
      for I in Digit_List'Range loop
         A1_Index := I - (Radix - N1.Radix);
         A2_Index := I - (Radix - N2.Radix);
         A1 := (if A1_Index >= 1 and then A1_Index <= N1.Digit_List'Last
                  then N1.Digit_List (A1_Index) else 0);
         A2 := (if A2_Index >= 1 and then A2_Index <= N2.Digit_List'Last
                  then N2.Digit_List (A2_Index) else 0);
         D_Sum := A1 + A2 + Carry;
         Digit_List (I) := D_Sum mod 10;
         Carry := (if D_Sum >= 10 then 1 else 0);
      end loop;
      --  Construct record
      N_Digits := Compute_MSD (Digit_List, Radix);
      return (N_Digits => N_Digits,
              MSD => N_Digits,
              Radix => Radix,
              Digit_List => Digit_List (1 .. N_Digits));
   end Add;

end Elementary_Arithmetic.Addition;
