with Bytes;
use Bytes;

--  TODO: make it so input size is not a constraint
--     => incremental processing, then finalize

generic
   N_Buckets : Positive := 128;
   Checksum_Length : Natural := 1;
   Min_Buffer_Length : Positive := 50;
package TLSH_Hash is
   pragma Assertion_Policy (Check);

   type Hash is tagged private;

   procedure From_Buffer (Self : in out Hash;
                          Buffer : Byte_String)
      with Pre => Buffer'Length >= Min_Buffer_Length;

   procedure From_File (Self : in out Hash;
                        File : String);

   function As_Hex (self : Hash) return String;

private

   type Checksum_Index is new Integer range 1 .. Checksum_Length;
   type Checksum_Bytes is array (Checksum_Index) of Byte;
   type Hash_Header is record
      Checksum : Checksum_Bytes := (others => 0);
      L_Value  : Byte := 0;
      Q_Ratio  : Full_Bite := (0, 0);
   end record;

   type Hash_Index is new Integer range 0 .. N_Buckets / 4 - 1;
   type Hash_Bytes is array (Hash_Index) of Byte;

   type Hash is tagged record
      Header : Hash_Header;
      Bytes : Hash_Bytes := (others => 0);
   end record
      with Type_Invariant => (
         (case N_Buckets is
            when 48 | 128 | 256 => True,
            when others => False) and then
         (case Checksum_Length is
            when 1 | 3 => True,
            when others => False));

end TLSH_Hash;
