with Bytes;
use Bytes;

package Pearson_Hash is

   function Compute (Salt : Byte;
                     I : Byte;
                     J : Byte;
                     K : Byte) return Byte;

end Pearson_Hash;
