with Pearson_Hash;

package body TLSH_Hash is

   procedure From_Buffer (Self : in out Hash;
                          Buffer : Byte_String)
   is
      type Window_Indices is new Integer range 1 .. 5;
      type Window is array (Window_Indices) of Byte;

      type Effective_Bucket_Indices is new Integer range 0 .. N_Buckets - 1;
      type Buckets is array (Byte) of Natural;

      B : Buckets := (others => 0);
      B_Index : Byte;
      W : Window;

      Q1, Q2, Q3 : Natural;
      Q1_Ratio : Float;
      Q2_Ratio : Float;

      C : array (Effective_Bucket_Indices) of Crumb;
      Ic : Effective_Bucket_Indices;
   begin
      for Start in Buffer'First .. Buffer'Last - 4 loop
         --  Step 1: Sliding window counts
         W := Window (Buffer (Start .. Start + 4));
         ---  6 / 10 possible triplets
         B_Index := Pearson_Hash.Compute (49,  W (1), W (2), W (3));
         B (B_Index) := B (B_Index) + 1;
         B_Index := Pearson_Hash.Compute (12,  W (1), W (2), W (4));
         B (B_Index) := B (B_Index) + 1;
         B_Index := Pearson_Hash.Compute (178, W (1), W (2), W (5));
         B (B_Index) := B (B_Index) + 1;
         B_Index := Pearson_Hash.Compute (166, W (1), W (3), W (4));
         B (B_Index) := B (B_Index) + 1;
         B_Index := Pearson_Hash.Compute (84,  W (1), W (3), W (5));
         B (B_Index) := B (B_Index) + 1;
         B_Index := Pearson_Hash.Compute (230, W (1), W (4), W (5));
         B (B_Index) := B (B_Index) + 1;
         --  NOTE: in the last window, we could consider all triplets
      end loop;
      --  Step 2: Compute quartiles
      --     Implement quickselect!
      --     Check that Q3 is not zero!
         Q1_Ratio := (Float (Q1) * 100.0) / Float (Q3);
         Q2_Ratio := (Float (Q2) * 100.0) / Float (Q3);
      --  Step 3: Construct digest header
         Self.Header.Checksum := (others => 0);
         Self.Header.L_Value := 0;
         Self.Header.Q_Ratio :=
            (High => Nibble (Integer (Float'Floor (Q1_Ratio)) rem 16),
             Low => Nibble (Integer (Float'Floor (Q2_Ratio)) rem 16));
      --  Step 4: Construct digest body
      for I in C'Range loop
         declare
            Bucket : constant Natural := B (Byte (I));
         begin
            if Bucket <= Q1 then
               C (I) := 0;
            elsif Bucket <= Q2 then
               C (I) := 1;
            elsif Bucket <= Q3 then
               C (I) := 2;
            else
               C (I) := 3;
            end if;
         end;
      end loop;
      --  Crumbs to bytes
      for I in Self.Bytes'Range loop
         Ic := Effective_Bucket_Indices (4 * I);
         Self.Bytes (I) := To_Byte (
            Crumble'(High_High => C (Ic),
                     High_Low  => C (Ic + 1),
                     Low_High  => C (Ic + 2),
                     Low_Low   => C (Ic + 3)));
      end loop;
   end From_Buffer;

   procedure From_File (Self : in out Hash;
                        File : String)
   is
   begin
      null;
   end From_File;

   function As_Hex (self : Hash) return String
   is
   begin
      return "";
   end As_Hex;

end TLSH_Hash;
