package Bytes with Pure is
   pragma Assertion_Policy (Check);

   type Half_Index is (High, Low);
   type Quarter_Index is (High_High, High_Low, Low_High, Low_Low);

   type Double_Word is mod 2 ** 64 with Size => 64;

   type Word is mod 2 ** 32 with Size => 32;
   type Word_Pair is array (Half_Index) of Word;

   type Half_Word is mod 2 ** 16 with Size => 16;
   type Half_Word_Pair is array (Half_Index) of Half_Word;

   type Byte is mod 2 ** 8 with Size => 8;
   type Byte_Pair is array (Half_Index) of Byte;
   type Byte_String is array (Natural range <>) of Byte;

   type Nibble is mod 2 ** 4 with Size => 4;
   type Full_Bite is array (Half_Index) of Nibble
      with Size => 8, Pack;

   type Crumb is mod 2 ** 2 with Size => 2;
   type Crumble is array (Quarter_Index) of Crumb
      with Size => 8, Pack;

   function To_Byte (N : Full_Bite) return Byte
      is (Byte (N (High)) * 16 + Byte (N (Low)));

   function To_Byte (C : Crumble) return Byte
      is (Byte (C (High_High)) * 64 + Byte (C (High_Low)) * 16 +
          Byte (C (Low_High)) * 4  + Byte (C (Low_Low)));

   function To_Full_Bite (B : Byte) return Full_Bite
      is (Full_Bite'(High => Nibble (B / 16),
                     Low => Nibble (B rem 16)))
         with Post => To_Byte (To_Full_Bite'Result) = B;

   function To_Crumble (B : Byte) return Crumble
      is (Crumble'(High_High => Crumb (B / 64),
                   High_Low => Crumb ((B rem 64) / 16),
                   Low_High => Crumb ((B rem 16) / 4),
                   Low_Low => Crumb (B rem 4)))
         with Post => To_Byte (To_Crumble'Result) = B;

end Bytes;
