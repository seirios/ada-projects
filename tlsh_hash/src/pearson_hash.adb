with PragmARC.Hash_Fast_Variable_Length;

package body Pearson_Hash is

   function Compute (Salt : Byte;
                     I : Byte;
                     J : Byte;
                     K : Byte) return Byte
   is
      type Byte_List is array (Natural range <>) of Byte;

      function Same_Byte (B : Byte)
         return PragmARC.Hash_Fast_Variable_Length.Byte
         is (PragmARC.Hash_Fast_Variable_Length.Byte (B));

      function Hash is new PragmARC.Hash_Fast_Variable_Length.Hash
         (Element => Byte,
          Index => Natural,
          String => Byte_List,
          To_Byte => Same_Byte);

      L : constant Byte_List := (Salt, I, J, K);
   begin
      return Byte (Hash (L));
   end Compute;

end Pearson_Hash;
