with TLSH_Hash;
with Bytes; use Bytes;

procedure Tests is
   S : constant Byte_String := (1, 2, 3, 4, 5, 1, 2, 3, 4, 5,
                                1, 2, 3, 4, 5, 1, 2, 3, 4, 5,
                                1, 2, 3, 4, 5, 1, 2, 3, 4, 5,
                                1, 2, 3, 4, 5, 1, 2, 3, 4, 5,
                                1, 2, 3, 4, 5, 1, 2, 3, 4, 5);
   package TLSH_Hash_128 is new TLSH_Hash
      (N_Buckets => 128);
   use TLSH_Hash_128;
   H : Hash;

   B : constant Byte := 132;
   N : constant Full_Bite := To_Full_Bite (B);
   C : constant Crumble := To_Crumble (B);
begin
   H.From_Buffer (S);
end Tests;
