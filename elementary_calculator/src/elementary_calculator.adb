with Ada.IO_Exceptions;
with Ada.Text_IO; use Ada.Text_IO;
with Elementary_Arithmetic; use Elementary_Arithmetic;
with Elementary_Arithmetic.Addition; use Elementary_Arithmetic.Addition;

procedure Elementary_Calculator is
   Unknown_Operation : exception;
begin
   Main : loop
      declare
         Op : constant String := Get_Line;
      begin
         if Op = "+" then
            declare
               S1 : constant String := Get_Line;
               N1 : constant Number := To_Number (S1);
               S2 : constant String := Get_Line;
               N2 : constant Number := To_Number (S2);
               N  : constant Number := Add (N1, N2);
            begin
               Print (N);
            end;
         elsif Op = "" then
            exit Main;
         else
            raise Unknown_Operation;
         end if;
      exception
         when Unknown_Operation => Put_Line ("Unknown operation '" & Op & "'");
         when Bad_Numeric_String =>
            Put_Line ("Bad numeric string");
      end;
   end loop Main;
exception
   when Ada.IO_Exceptions.End_Error => null;
end Elementary_Calculator;
