with Ada.Strings.Unbounded;
with BNF_Parsers;

package BNF_Parser is
   package UStrings renames Ada.Strings.Unbounded;

   Unknown_Expression : exception;

   type Expression_Kind is (Symbol, Literal, Alternation);

   type Expression (Kind : Expression_Kind := Symbol) is record
      case Kind is
      when Symbol =>
         Symbol : UStrings.Unbounded_String;
      when Literal =>
         Literal : UStrings.Unbounded_String;
      when Alternation =>
         Left : access Expression;
         Right : access Expression;
      end case;
   end record;

   package Parser is new BNF_Parsers
      (Expression_Type => Expression);

   procedure Parse (P : in out Parser.BNF_Parser;
      File_Name : String);

end BNF_Parser;
