with Ada.Strings.Unbounded;
with Ada.Containers.Indefinite_Vectors;
with Ada.Containers.Indefinite_Ordered_Maps;

generic
   type Expression_Type is private;
package BNF_Parsers is
   package UStrings renames Ada.Strings.Unbounded;

   type BNF_Parser is tagged private;

private

   package Expression_Vectors is new Ada.Containers.Indefinite_Vectors
      (Index_Type => Natural,
       Element_Type => Expression_Type);

   type Production_Rule is record
      Tag : UStrings.Unbounded_String;
      Expression : Expression_Vectors.Vector;
   end record;

   package Production_Rule_Maps is new Ada.Containers.Indefinite_Ordered_Maps
      (Key_Type => String,
       Element_Type => Production_Rule);

   type BNF_Parser is tagged record
      Syntax : Production_Rule_Maps.Map;
   end record;

end BNF_Parsers;
