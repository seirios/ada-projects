with Ada.Text_IO;
with Ada.Integer_Text_IO;

package body Generic_Binary_Symmetric_Matrix is

   procedure Init_Order (X : out Order) is
   begin
      for I in Indexer loop
         X (I) := I;
      end loop;
   end Init_Order;

   function Swap_Order (X : Order; IX_A : Indexer; IX_B : Indexer)
      return Order is
      T : Indexer;
      O_Swap : Order := X;
   begin
      T := O_Swap (IX_A);
      O_Swap (IX_A) := O_Swap (IX_B);
      O_Swap (IX_B) := T;
      return O_Swap;
   end Swap_Order;

   procedure Print_Order (X : Order) is
      use Ada.Text_IO;
      use Ada.Integer_Text_IO;
   begin
      for I in Indexer loop
         if I > Indexer'First then
            Put (" ");
         end if;
         Put (X (I), Width => 0);
      end loop;
      New_Line;
   end Print_Order;

   procedure Clear (M : out Matrix) is
   begin
      for I in UT_Indexer loop
         M (I) := Unset;
      end loop;
   end Clear;

   procedure Set_Entry (M : in out Matrix; E : Entry_Index; B : Bit_Option) is
      Y : constant UT_Indexer := Entry_To_Index (E);
   begin
      M (Y) := B;
   end Set_Entry;

   function Get_Entry (M : Matrix; E : Entry_Index)
      return Bit_Option is
      Y : constant UT_Indexer := Entry_To_Index (E);
   begin
      return M (Y);
   end Get_Entry;

   procedure Print_Matrix (M : Matrix) is
      use Ada.Text_IO;
   begin
      for I in Indexer loop
         for J in Indexer loop
            if J < I then
               Put (" ");
            else
               case Get_Entry (M, Entry_Index'(Row => I, Column => J)) is
                  when Unset => Put ("X");
                  when Zero => Put ("0");
                  when One => Put ("1");
               end case;
            end if;
            Put (" ");
         end loop;
         New_Line;
      end loop;
      New_Line;
   end Print_Matrix;

   function Swap (M : Matrix; IX_A, IX_B : Indexer)
      return Matrix is
      M_Swap : Matrix;
      E : Entry_Index;
      B : Bit_Option;
      T : Indexer;
   begin
      Clear (M_Swap);
      for R in Indexer loop
         for C in R + 1 .. Indexer'Last loop
            E := (Row => R, Column => C);
            B := Get_Entry (M, E);
            --  Swap row index
            if E (Row) = IX_A then
               E (Row) := IX_B;
            elsif E (Row) = IX_B then
               E (Row) := IX_A;
            end if;
            --  Swap column index
            if E (Column) = IX_A then
               E (Column) := IX_B;
            elsif E (Column) = IX_B then
               E (Column) := IX_A;
            end if;
            --  Make upper triangular
            if E (Column) < E (Row) then
               T := E (Row);
               E (Row) := E (Column);
               E (Column) := T;
            end if;
            Set_Entry (M_Swap, E, B);
         end loop;
      end loop;
      return M_Swap;
   end Swap;

   function Is_Doubly_Symmetric (M : Matrix)
      return Boolean is
      E : Entry_Index;
      E_Sym : Entry_Index;
   begin
      for R in 1 .. (N - 1) / 2 loop
         for C in R + 1 .. N - R loop
            E := (Row => R, Column => C);
            E_Sym := (Row => N - (C - 1), Column => N - (R - 1));
            if Get_Entry (M, E) /= Get_Entry (M, E_Sym) then
               return False;
            end if;
         end loop;
      end loop;
      return True;
   end Is_Doubly_Symmetric;

--  private

   function Entry_To_Index (E : Entry_Index)
      return UT_Indexer is
      R : Indexer := E (Row);
      C : Indexer := E (Column);
   begin
      if E (Column) < E (Row) then
         --  Swap to make upper triangular
         R := E (Column);
         C := E (Row);
      end if;
      return C + Indexer'Last * (R - 1) - (R * (R - 1)) / 2;
   end Entry_To_Index;

end Generic_Binary_Symmetric_Matrix;
