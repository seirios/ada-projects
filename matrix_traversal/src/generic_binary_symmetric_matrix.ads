--------------------------------------------------
--  Generic Binary Symmetric Matrix (N x N)
--  Implementation Notes:
--   - Upper triangular part is stored as a Vector
--   - Values are an enumeration (Unset, 0, 1)
--------------------------------------------------
generic
   N : Positive;
package Generic_Binary_Symmetric_Matrix is
   type Matrix is private;
   type Bit_Option is (Unset, Zero, One);
   type Entry_Name is (Row, Column);
   subtype Indexer is Positive range 1 .. N;
   type Entry_Index is array (Entry_Name) of Indexer;
   type Order is array (Indexer) of Indexer;

   procedure Init_Order (X : out Order);

   function Swap_Order (X : Order; IX_A : Indexer; IX_B : Indexer)
      return Order;

   procedure Print_Order (X : Order);

   --  Clear: unset all values
   procedure Clear (M : out Matrix);

   --  Set specific entry
   procedure Set_Entry (M : in out Matrix;
                        E : Entry_Index;
                        B : Bit_Option);

   --  Get specific entry
   function Get_Entry (M : Matrix;
                       E : Entry_Index)
      return Bit_Option;

   --  Pretty print matrix
   procedure Print_Matrix (M : Matrix);

   --  Swap two indices
   function Swap (M : Matrix; IX_A, IX_B : Indexer)
      return Matrix;

   --  Check is doubly symmetric
   function Is_Doubly_Symmetric (M : Matrix)
      return Boolean;

private

   N_Elem : constant Positive := (N * (N + 1)) / 2;
   subtype UT_Indexer is Positive range 1 .. N_Elem;
   type Matrix is array (UT_Indexer) of Bit_Option;

   function Entry_To_Index (E : Entry_Index)
      return UT_Indexer;

end Generic_Binary_Symmetric_Matrix;
