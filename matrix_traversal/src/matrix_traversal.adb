with Generic_Binary_Symmetric_Matrix;
with Ada.Containers.Vectors;

-----------------------------
--  Matrix_Traversal
--  Find unique symmetric ways to traverse a 2D image
--  based on shuffling of its adjacency matrix
-----------------------------
procedure Matrix_Traversal is

   --  Image size N x N
   N : constant Positive := 3;

   package Binary_Symmetric_Matrix is
      new Generic_Binary_Symmetric_Matrix (N => N * N);
   use Binary_Symmetric_Matrix;

   package Matrix_Vectors is
      new Ada.Containers.Vectors (Index_Type => Positive,
                                  Element_Type => Matrix);
   use Matrix_Vectors;

   --  Fill matrix with adjacency matrix derived from
   --  4-neighbor neighborhood structure (without wrap-around)
   procedure Init_Adjacency_Matrix (M : out Matrix) is
      R_A : Indexer;
      C_A : Indexer;
      R_B : Indexer;
      C_B : Indexer;
      DX : Integer;
      DY : Integer;
      B : Bit_Option;
      E : Entry_Index;
   begin
      Clear (M);
      for IX_A in 1 .. N * N loop
         R_A := 1 + (IX_A - 1) / N;
         C_A := 1 + (IX_A - 1) rem N;
         for IX_B in IX_A + 1 .. N * N loop
            R_B := 1 + (IX_B - 1) / N;
            C_B := 1 + (IX_B - 1) rem N;
            DX := (C_B - C_A);
            DY := (R_B - R_A);
            B := (if abs (DX) + abs (DY) = 1 then One else Zero);
            E := (Row => IX_A, Column => IX_B);
            Set_Entry (M, E, B);
         end loop;
      end loop;
   end Init_Adjacency_Matrix;

   V : Vector;
   M : Matrix;
   O : Order;
   M_Swap : Matrix;
   O_Swap : Order;
begin
   Init_Order (O);
   Init_Adjacency_Matrix (M);
   V.Append (M);

   Print_Order (O);
   Print_Matrix (M);
   --  Enumerate possible swaps
   for I in 1 .. N * N loop
      for J in I + 1 .. N * N loop
         O_Swap := Swap_Order (O, I, J);
         M_Swap := Swap (M, I, J);
         --  Keep matrix...
         if Is_Doubly_Symmetric (M_Swap) and then  --  ...if doubly symmetric
            V.Find_Index (M_Swap) = No_Index       --  ...and not seen before
         then
            V.Append (M_Swap);
            Print_Order (O_Swap);
            Print_Matrix (M_Swap);
         end if;
      end loop;
   end loop;
end Matrix_Traversal;
