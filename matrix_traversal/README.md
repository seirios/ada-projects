# Matrix traversal

My first code in Ada :)

It searches for unique symmetric ways of traversing an NxN image by shuffling the adjacency matrix of the individual pixels, considered as a graph with the usual neighborhood relation (4-neighbor neighborhood). Since it iterates over all shuffles of an N^2xN^2 matrix, this is only feasible for small N.

Additionally, plots of the unique traversals are generated automatically with `gnuplot`.

I got the idea for doing this at the Ada Europe Conference 2024 and then I implemented it quite straightforwardly in Ada during the weekend. Setting up the toolchain and build environment was a breeze thanks to Alire!
