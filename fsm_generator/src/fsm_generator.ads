with Ada.Strings.Unbounded;

with Ada.Containers.Ordered_Maps;
with Ada.Containers.Indefinite_Ordered_Maps;
with Ada.Containers.Indefinite_Vectors;
with Ada.Containers.Vectors;

generic
   type State_Type is (<>);
package FSM_Generator is
   package UStrings renames Ada.Strings.Unbounded;

   Tab : constant String := "   ";

   --  We use ASCII.LF only as sentinel to break lines,
   --  see Print_Line_By_Line
   function "+" (Left, Right : String) return String
      is (Left & ASCII.LF & Right);

   Current_State : constant String := "S";
   Previous_State : constant String := "S_Prev";
   Input_Buffer : constant String := "Input";
   Input_Element : constant String := "T";
   Input_Index : constant String := "I";
   EOF : constant String := "";

   type Action_Kind is (Accepting, Rejecting, Custom);
   type Transition_Kind is (Other_States, Same_State, All_States);
   type Parameter_Kind is (Param_In, Param_Out, Param_In_Out);
   type Iteration_Kind is (By_Element, By_Index);
   type Conditional_Kind is (As_Case, As_If_Else);

   type FSM_Spec is tagged limited private;

   --  Constructors
   function Init (Name : String;
      Initial_State : State_Type := State_Type'First)
      return FSM_Spec;

   --  Exceptions
   Unknown_State   : exception;
   Invalid_Spec    : exception;
   Incomplete_Spec : exception;

   --  Methods
   procedure Add_Transition (Self : in out FSM_Spec;
                             From_State : State_Type;
                             On_Symbol : String;
                             To_State : String;
                             Kind : Action_Kind := Custom;
                             Action : String := "");
   procedure Add_Transition (Self : in out FSM_Spec;
                             From_State : State_Type;
                             On_Symbol : String;
                             To_State : State_Type;
                             Kind : Action_Kind := Custom;
                             Action : String := "");
   procedure Set_Transition_Is_Total (Self : in out FSM_Spec;
                                      State : State_Type);
   procedure Add_State_Entry_Action (Self : in out FSM_Spec;
                                     State : State_Type;
                                     Action : String;
                                     Kind : Transition_Kind := Other_States);
   procedure Add_State_Leave_Action (Self : in out FSM_Spec;
                                     State : State_Type;
                                     Action : String;
                                     Kind : Transition_Kind := Other_States);
   procedure Add_Extra_Parameter (Self : in out FSM_Spec;
                                  Name : String;
                                  Tipo : String;
                                  Init : String := "";
                                  Kind : Parameter_Kind := Param_Out);
   procedure Add_Extra_Variable (Self : in out FSM_Spec;
                                 Name : String;
                                 Tipo : String;
                                 Init : String := "");
   procedure Add_Extra_State_Variable (Self : in out FSM_Spec;
                                       Name : String);

   procedure Setup_Result (Self : in out FSM_Spec;
                           Result_Type : String := "Boolean";
                           Result_Name : String := "";
                           Result_Init : String := "");
   procedure Setup_Input (Self : in out FSM_Spec;
                          Input_Type       : String := "String";
                          Iteration_Mode   : Iteration_Kind := By_Element;
                          Conditional_Mode : Conditional_Kind := As_Case);

   procedure Generate (Self : FSM_Spec;
                       File_Name : String := "");

   --  Getters
   function Name (Self : FSM_Spec) return String;
   function Initial_State (Self : FSM_Spec) return String;
   function Accepting_Action (Self : FSM_Spec) return String;
   function Rejecting_Action (Self : FSM_Spec) return String;

   --  Setters
   procedure Name (Self : in out FSM_Spec;
                   Name : String);
   procedure Initial_State (Self : in out FSM_Spec;
                            State : State_Type);
   procedure Accepting_Action (Self : in out FSM_Spec;
                               S : String);
   procedure Rejecting_Action (Self : in out FSM_Spec;
                               S : String);

   --  Debug
   procedure Print_Name (Self : FSM_Spec);
   procedure Print_States (Self : FSM_Spec);
   procedure Print_Transitions (Self : FSM_Spec);

private

   --  Private types
   package String_Vectors is
      new Ada.Containers.Indefinite_Vectors
         (Index_Type => Natural,
         Element_Type => String);

   type State_Action is record
      Kind   : Transition_Kind;
      Action : UStrings.Unbounded_String;
   end record;

   package State_Action_Vectors is
      new Ada.Containers.Vectors
         (Index_Type => Natural,
          Element_Type => State_Action);

   type Parameter is record
      Kind : Parameter_Kind := Param_Out;
      Name : UStrings.Unbounded_String;
      Tipo : UStrings.Unbounded_String;
      Init : UStrings.Unbounded_String;
   end record;

   package Parameter_Vectors is
      new Ada.Containers.Vectors
         (Index_Type => Natural,
         Element_Type => Parameter);

   type Transition is record
      Is_Set   : Boolean := False;
      To_State : UStrings.Unbounded_String;
      Action   : UStrings.Unbounded_String;
   end record;

   package Transition_Maps is
      new Ada.Containers.Indefinite_Ordered_Maps
         (Key_Type        => String,
          Element_Type    => Transition);

   type Transition_Table is record
      Is_Total : Boolean := False;
      Map      : Transition_Maps.Map;
      On_EOF   : Transition;
      Entry_Action_List : State_Action_Vectors.Vector;
      Leave_Action_List : State_Action_Vectors.Vector;
   end record;

   package Transition_Table_Maps is
      new Ada.Containers.Ordered_Maps
         (Key_Type        => State_Type,
          Element_Type    => Transition_Table);

   type FSM_Spec is tagged limited record
      Name             : UStrings.Unbounded_String;
      Initial_State    : State_Type;
      Transitions      : Transition_Table_Maps.Map;

      Extra_Parameters : Parameter_Vectors.Vector;
      Extra_Variables  : Parameter_Vectors.Vector;

      Iteration_Mode   : Iteration_Kind := By_Element;
      Conditional_Mode : Conditional_Kind := As_Case;

      Input_Type       : UStrings.Unbounded_String :=
         UStrings.To_Unbounded_String ("String");
      Result_Type      : Parameter := (
         Tipo => UStrings.To_Unbounded_String ("Boolean"),
         Name => UStrings.To_Unbounded_String (""),
         Init => UStrings.To_Unbounded_String (""),
         others => <>);
      Accepting_Action : UStrings.Unbounded_String :=
         UStrings.To_Unbounded_String ("return True;");
      Rejecting_Action : UStrings.Unbounded_String :=
         UStrings.To_Unbounded_String ("return False;");
   end record;

end FSM_Generator;
