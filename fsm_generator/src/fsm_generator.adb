with Ada.Text_IO;
with Ada.Strings;
with Ada.Strings.Maps;
with Ada.Strings.Fixed;

package body FSM_Generator is

   --  Utility functions
   procedure Check_Identifier (S : String)
   is
      use Ada.Strings.Maps;
   begin
      if S = "" then
         raise Invalid_Spec with "Name must not be empty";
      end if;
      if Is_In (' ', To_Set (S)) then
         raise Invalid_Spec with
            "Name '" & S & "' must not contain spaces";
      end if;
   end Check_Identifier;

   function Check_State (S : String) return Boolean
   is
      State : State_Type;
   begin
      State := State_Type'Value (S);
      return True;
   exception
      when Constraint_Error =>
         return False;
   end Check_State;

   --  Constructors
   function Init (Name : String;
      Initial_State : State_Type := State_Type'First)
      return FSM_Spec
   is
      Transitions : Transition_Table_Maps.Map;
   begin
      Check_Identifier (Name);
      for S in State_Type loop
         Transitions.Insert (S, (others => <>));
      end loop;
      return (Name => UStrings.To_Unbounded_String (Name),
              Initial_State => Initial_State,
              Transitions => Transitions,
              others => <>);
   end Init;

   --  Methods
   procedure Add_Transition (Self : in out FSM_Spec;
      From_State : State_Type;
      On_Symbol : String;
      To_State : String;
      Kind : Action_Kind := Custom;
      Action : String := "")
   is
      function Check_Extra_State_Variable (S : String)
         return Boolean is
      begin
         for V of Self.Extra_Variables loop
            if UStrings.To_String (V.Tipo) = "State"
               and then UStrings.To_String (V.Name) = S
            then
               return True;
            end if;
         end loop;
         return False;
      end Check_Extra_State_Variable;

      Action_String : constant UStrings.Unbounded_String :=
         (case Kind is
            when Accepting => Self.Accepting_Action,
            when Rejecting => Self.Rejecting_Action,
            when Custom => UStrings.To_Unbounded_String (Action));
   begin
      if not Check_State (To_State)
         and then To_State /= Previous_State
         and then not Check_Extra_State_Variable (To_State)
      then
         raise Unknown_State with "Unknown state '" & To_State & "'";
      end if;
      if On_Symbol = EOF then
         Self.Transitions (From_State).On_EOF :=
            (Is_Set => True,
             To_State => UStrings.To_Unbounded_String (To_State),
             Action => Action_String);
      else
         Self.Transitions (From_State).Map.Insert (On_Symbol,
            (Is_Set => True,
             To_State => UStrings.To_Unbounded_String (To_State),
             Action => Action_String));
      end if;
   end Add_Transition;

   procedure Add_Transition (Self : in out FSM_Spec;
      From_State : State_Type;
      On_Symbol : String;
      To_State : State_Type;
      Kind : Action_Kind := Custom;
      Action : String := "")
   is
   begin
      Self.Add_Transition (From_State, On_Symbol, To_State'Image,
                           Kind, Action);
   end Add_Transition;

   procedure Set_Transition_Is_Total (Self : in out FSM_Spec;
      State : State_Type)
   is
   begin
      Self.Transitions (State).Is_Total := True;
   end Set_Transition_Is_Total;

   procedure Add_State_Entry_Action (Self : in out FSM_Spec;
      State : State_Type;
      Action : String;
      Kind : Transition_Kind := Other_States)
   is
   begin
      Self.Transitions (State).Entry_Action_List.Append (
         (Kind => Kind,
          Action => UStrings.To_Unbounded_String (Action)));
   end Add_State_Entry_Action;

   procedure Add_State_Leave_Action (Self : in out FSM_Spec;
      State : State_Type;
      Action : String;
      Kind : Transition_Kind := Other_States)
   is
   begin
      Self.Transitions (State).Leave_Action_List.Append (
         (Kind => Kind,
          Action => UStrings.To_Unbounded_String (Action)));
   end Add_State_Leave_Action;

   procedure Add_Extra_Parameter (Self : in out FSM_Spec;
      Name : String;
      Tipo : String;
      Init : String := "";
      Kind : Parameter_Kind := Param_Out)
   is
   begin
      if Kind = Param_In and then Init /= "" then
         raise Invalid_Spec with "Cannot initialize read-only parameter";
      end if;
      Check_Identifier (Name);
      Check_Identifier (Tipo);
      Self.Extra_Parameters.Append ((
         Kind => Kind,
         Name => UStrings.To_Unbounded_String (Name),
         Tipo => UStrings.To_Unbounded_String (Tipo),
         Init => UStrings.To_Unbounded_String (Init)));
   end Add_Extra_Parameter;

   procedure Add_Extra_Variable (Self : in out FSM_Spec;
      Name : String;
      Tipo : String;
      Init : String := "")
   is
   begin
      Check_Identifier (Name);
      Check_Identifier (Tipo);
      Self.Extra_Variables.Append ((
         Name => UStrings.To_Unbounded_String (Name),
         Tipo => UStrings.To_Unbounded_String (Tipo),
         Init => UStrings.To_Unbounded_String (Init),
         others => <>));
   end Add_Extra_Variable;

   procedure Add_Extra_State_Variable (Self : in out FSM_Spec;
      Name : String)
   is
   begin
      Self.Add_Extra_Variable (Name, "State");
   end Add_Extra_State_Variable;

   procedure Setup_Result (Self : in out FSM_Spec;
      Result_Type : String := "Boolean";
      Result_Name : String := "";
      Result_Init : String := "")
   is
   begin
      if Result_Type = "" and then Result_Name /= "" then
         raise Invalid_Spec with "Procedure cannot have a result variable";
      end if;
      if Result_Name = "" and then Result_Init /= "" then
         raise Invalid_Spec with "There is no result variable to initialize";
      end if;
      Check_Identifier (Result_Type);
      Check_Identifier (Result_Name);
      Self.Result_Type := (
         Tipo => UStrings.To_Unbounded_String (Result_Type),
         Name => UStrings.To_Unbounded_String (Result_Name),
         Init => UStrings.To_Unbounded_String (Result_Init),
         others => <>);
   end Setup_Result;

   procedure Setup_Input (Self : in out FSM_Spec;
                          Input_Type       : String := "String";
                          Iteration_Mode   : Iteration_Kind := By_Element;
                          Conditional_Mode : Conditional_Kind := As_Case)
   is
   begin
      Check_Identifier (Input_Type);
      Self.Input_Type := UStrings.To_Unbounded_String (Input_Type);
      Self.Iteration_Mode := Iteration_Mode;
      Self.Conditional_Mode := Conditional_Mode;
   end Setup_Input;

   procedure Generate (Self : FSM_Spec;
      File_Name : String := "")
   is
      use Ada.Text_IO;
      use Ada.Strings.Fixed;

      function Transition_Table_Key
         (Position : Transition_Table_Maps.Cursor) return State_Type
         renames Transition_Table_Maps.Key;
      function Transition_Key
         (Position : Transition_Maps.Cursor) return String
         renames Transition_Maps.Key;

      function Is_Empty (S : UStrings.Unbounded_String)
         return Boolean
         is (UStrings.To_String (S) = "");

      function Has_Any_State_Entry_Action return Boolean is
      begin
         for S in Self.Transitions.Iterate loop
            if not Self.Transitions (S).Entry_Action_List.Is_Empty then
               return True;
            end if;
         end loop;
         return False;
      end Has_Any_State_Entry_Action;

      function Has_All_State_Entry_Actions return Boolean is
      begin
         for S in Self.Transitions.Iterate loop
            if Self.Transitions (S).Entry_Action_List.Is_Empty then
               return False;
            end if;
         end loop;
         return True;
      end Has_All_State_Entry_Actions;

      function Has_Any_State_Leave_Action return Boolean is
      begin
         for S in Self.Transitions.Iterate loop
            if not Self.Transitions (S).Leave_Action_List.Is_Empty then
               return True;
            end if;
         end loop;
         return False;
      end Has_Any_State_Leave_Action;

      function Has_All_State_Leave_Actions return Boolean is
      begin
         for S in Self.Transitions.Iterate loop
            if Self.Transitions (S).Leave_Action_List.Is_Empty then
               return False;
            end if;
         end loop;
         return True;
      end Has_All_State_Leave_Actions;

      function Has_All_EOF_Action return Boolean is
      begin
         for S in Self.Transitions.Iterate loop
            if not Self.Transitions (S).On_EOF.Is_Set then
               return False;
            end if;
         end loop;
         return True;
      end Has_All_EOF_Action;

      procedure Put_Line_By_Line (S : String;
         Indent : String := "")
      is
         use Ada.Strings;
         use Ada.Strings.Maps;
         Separator : constant Character_Set := To_Set (ASCII.LF);
         F : Positive;
         L : Natural;
         I : Natural := 1;
      begin
         while I in S'Range loop
            Find_Token (Source => S,
                        Set => Separator,
                        From => I,
                        Test => Outside,
                        First => F,
                        Last => L);

            exit when L = 0;

            Put_Line (Indent & S (F .. L));

            I := L + 1;
         end loop;
      end Put_Line_By_Line;

      Rejecting_Action : constant String :=
         UStrings.To_String (Self.Rejecting_Action);
      Result_Type  : constant String :=
         UStrings.To_String (Self.Result_Type.Tipo);
      Result_Name  : constant String :=
         UStrings.To_String (Self.Result_Type.Name);
      Result_Init  : constant String :=
         UStrings.To_String (Self.Result_Type.Init);
      Input_Type   : constant String :=
         UStrings.To_String (Self.Input_Type);

      F : File_Type;
   begin
      if File_Name /= "" then
         Create (F, Out_File, File_Name);
         Set_Output (F);
      end if;

      Put_Line ("--  Generated with FSM_Generator");
      if Result_Type = "" then
         Put ("procedure ");
      else
         Put ("function ");
      end if;
      Put (UStrings.To_String (Self.Name));
      Put (" (" & Input_Buffer & " : " & Input_Type);
      if not Self.Extra_Parameters.Is_Empty then
         for P of Self.Extra_Parameters loop
            Put (";");
            New_Line;
            Put (Tab & UStrings.To_String (P.Name)
                 & " : "
                 & (case P.Kind is
                       when Param_In => "in",
                       when Param_Out => "out",
                       when Param_In_Out => "in out")
                 & " "
                 & UStrings.To_String (P.Tipo));
         end loop;
      end if;
      Put (")");
      if Result_Type /= "" then
         Put_Line (" return " & Result_Type);
      end if;
      Put_Line ("is");
      if Result_Name /= "" then
         Put (Tab & Result_Name
              & " : " & Result_Type);
         if Result_Init /= "" then
            Put (" := " & Result_Init);
         end if;
         Put_Line (";");
      end if;
      Put (Tab & "type State is (");
      declare
         N : Natural := 1;
      begin
         for S in State_Type loop
            Put (S'Image);
            if S /= State_Type'Last then
               Put (",");
            end if;
            if N mod 3 = 0 then
               New_Line;
               Put (2 * Tab);
            elsif S /= State_Type'Last then
               Put (" ");
            end if;
            N := N + 1;
         end loop;
      end;
      Put_Line (");");
      Put_Line (Tab & Current_State & " : State := "
                & Self.Initial_State'Image & ";");
      Put_Line (Tab & Previous_State & " : State;");
      for V of Self.Extra_Variables loop
         Put (Tab
              & UStrings.To_String (V.Name)
              & " : "
              & UStrings.To_String (V.Tipo));
         if not Is_Empty (V.Init) then
            Put (" := "
                 & UStrings.To_String (V.Init));
         end if;
         Put_Line (";");
      end loop;
      Put_Line ("begin");
      --  Initialize extra parameters
      if not Self.Extra_Parameters.Is_Empty then
         Put_Line (Tab & "--  Handle initialization of extra parameters");
         for P of Self.Extra_Parameters loop
            if not Is_Empty (P.Init) then
               Put_Line (Tab & UStrings.To_String (P.Name)
                         & " := "
                         & UStrings.To_String (P.Init) & ";");
            end if;
         end loop;
      end if;
      --  Iterate over input
      Put_Line (Tab & "--  Iterate over input");
      case Self.Iteration_Mode is
      when By_Index =>
         Put_Line (Tab & "for " & Input_Index
                   & " in " & Input_Buffer & "'Range loop");
      when By_Element =>
         Put_Line (Tab & "for " & Input_Element
                   & " of " & Input_Buffer & " loop");
      end case;
      --  Save previous state
      Put_Line (2 * Tab & Previous_State
                & " := " & Current_State & ";");
      --  Transitions
      Put_Line (2 * Tab & "case " & Current_State & " is");
      for S in Self.Transitions.Iterate loop
         Put_Line (2 * Tab & "when "
                   & Transition_Table_Key (S)'Image & " =>");
         case Self.Conditional_Mode is
         when As_Case =>
            case Self.Iteration_Mode is
            when By_Index =>
               Put_Line (3 * Tab & "case " & Input_Buffer
                         & " (" & Input_Index & ") is");
            when By_Element =>
               Put_Line (3 * Tab & "case " & Input_Element & " is");
            end case;
         when As_If_Else => null;
         end case;

         declare
            use Transition_Maps;
            State_Transitions : Transition_Maps.Map
               renames Self.Transitions (S).Map;
         begin
            for T in State_Transitions.Iterate loop
               case Self.Conditional_Mode is
               when As_Case =>
                  Put_Line (3 * Tab & "when " & Transition_Key (T) & " =>");
               when As_If_Else =>
                  if T = State_Transitions.Iterate.First then
                     case Self.Iteration_Mode is
                     when By_Index =>
                        Put_Line (3 * Tab &  "if " & Input_Buffer
                                  & " (" & Input_Index & ") = "
                                  & Transition_Key (T) & " then");
                     when By_Element =>
                        Put_Line (3 * Tab &  "if " & Input_Element
                                  & " = " & Transition_Key (T) & " then");
                     end case;
                  else
                     case Self.Iteration_Mode is
                     when By_Index =>
                        Put_Line (3 * Tab &  "elsif " & Input_Buffer
                                  & " (" & Input_Index & ") = "
                                  & Transition_Key (T) & " then");
                     when By_Element =>
                        Put_Line (3 * Tab &  "elsif " & Input_Element
                                  & " = " & Transition_Key (T) & " then");
                     end case;
                  end if;
               end case;
               --  Action (optional)
               if not Is_Empty (State_Transitions (T).Action) then
                  Put_Line_By_Line (UStrings.To_String
                     (State_Transitions (T).Action), 4 * Tab);
               end if;
               --  State change
               Put_Line (4 * Tab & Current_State & " := "
                         & UStrings.To_String (State_Transitions (T).To_State)
                         & ";");
            end loop;
         end;
         if not Self.Transitions (S).Is_Total then
            case Self.Conditional_Mode is
            when As_Case =>
               Put_Line (3 * Tab & "when others =>");
            when As_If_Else =>
               Put_Line (3 * Tab & "else");
            end case;
            Put_Line_By_Line (Rejecting_Action, 4 * Tab);
         end if;
         case Self.Conditional_Mode is
         when As_Case =>
            Put_Line (3 * Tab & "end case;");
         when As_If_Else =>
            Put_Line (3 * Tab & "end if;");
         end case;
      end loop;
      Put_Line (2 * Tab & "end case;");
      --  State leave actions
      if Has_Any_State_Leave_Action then
         Put_Line (2 * Tab & "--  Handle state leave actions");
         Put_Line (2 * Tab & "case " & Previous_State & " is");
         for S in Self.Transitions.Iterate loop
            if not Self.Transitions (S).Leave_Action_List.Is_Empty then
               Put_Line (2 * Tab & "when "
                         & Transition_Table_Key (S)'Image
                         & " =>");
               for A of Self.Transitions (S).Leave_Action_List loop
                  case A.Kind is
                  when Other_States =>
                     Put_Line (3 * Tab & "if " & Previous_State
                               & " /= " & Current_State & " then");
                  when Same_State =>
                     Put_Line (3 * Tab & "if " & Previous_State
                               & " = " & Current_State & " then");
                  when All_States => null;
                  end case;

                  case A.Kind is
                  when Other_States | Same_State =>
                     Put_Line_By_Line (UStrings.To_String (A.Action),
                                       4 * Tab);
                     Put_Line (3 * Tab & "end if;");
                  when All_States =>
                     Put_Line_By_Line (UStrings.To_String (A.Action),
                                       3 * Tab);
                  end case;
               end loop;
            end if;
         end loop;
         if not Has_All_State_Leave_Actions then
            Put_Line (2 * Tab & "when others => null;");
         end if;
         Put_Line (2 * Tab & "end case;");
      end if;
      --  State entry actions
      if Has_Any_State_Entry_Action then
         Put_Line (2 * Tab & "--  Handle state entry actions");
         Put_Line (2 * Tab & "case " & Current_State & " is");
         for S in Self.Transitions.Iterate loop
            if not Self.Transitions (S).Entry_Action_List.Is_Empty then
               Put_Line (2 * Tab & "when "
                         & Transition_Table_Key (S)'Image
                         & " =>");
               for A of Self.Transitions (S).Entry_Action_List loop
                  case A.Kind is
                  when Other_States =>
                     Put_Line (3 * Tab & "if " & Current_State
                               & " /= " & Previous_State & " then");
                  when Same_State =>
                     Put_Line (3 * Tab & "if " & Current_State
                               & " = " & Previous_State & " then");
                  when All_States => null;
                  end case;

                  case A.Kind is
                  when Other_States | Same_State =>
                     Put_Line_By_Line (UStrings.To_String (A.Action),
                                       4 * Tab);
                     Put_Line (3 * Tab & "end if;");
                  when All_States =>
                     Put_Line_By_Line (UStrings.To_String (A.Action),
                                       3 * Tab);
                  end case;
               end loop;
            end if;
         end loop;
         if not Has_All_State_Entry_Actions then
            Put_Line (2 * Tab & "when others => null;");
         end if;
         Put_Line (2 * Tab & "end case;");
      end if;
      Put_Line (Tab & "end loop;");
      --  EOF symbol
      Put_Line (Tab & "--  Handle EOF");
      Put_Line (Tab & "case " & Current_State & " is");
      for S in Self.Transitions.Iterate loop
         if Self.Transitions (S).On_EOF.Is_Set then
            Put_Line (Tab & "when "
                      & Transition_Table_Key (S)'Image
                      & " =>");
            Put_Line_By_Line (UStrings.To_String
               (Self.Transitions (S).On_EOF.Action), 2 * Tab);
         end if;
      end loop;
      if not Has_All_EOF_Action then
         Put_Line (Tab & "when others =>");
         Put_Line (2 * Tab & UStrings.To_String (Self.Rejecting_Action));
      end if;
      Put_Line (Tab & "end case;");
      Put_Line ("end " & UStrings.To_String (Self.Name) & ";");

      if File_Name /= "" then
         Close (F);
         Set_Output (Standard_Output);
      end if;
   end Generate;

   --  Getters
   function Name (Self : FSM_Spec)
      return String
   is (UStrings.To_String (Self.Name));

   function Initial_State (Self : FSM_Spec)
      return String
   is (Self.Initial_State'Image);

   function Accepting_Action (Self : FSM_Spec)
      return String
   is (UStrings.To_String (Self.Accepting_Action));

   function Rejecting_Action (Self : FSM_Spec)
      return String
   is (UStrings.To_String (Self.Rejecting_Action));

   --  Setters
   procedure Name (Self : in out FSM_Spec;
      Name : String)
   is
   begin
      Check_Identifier (Name);
      Self.Name := UStrings.To_Unbounded_String (Name);
   end Name;

   procedure Initial_State (Self : in out FSM_Spec;
      State : State_Type)
   is
   begin
      Self.Initial_State := State;
   end Initial_State;

   procedure Accepting_Action (Self : in out FSM_Spec;
      S : String)
   is
   begin
      Self.Accepting_Action := UStrings.To_Unbounded_String (S);
   end Accepting_Action;

   procedure Rejecting_Action (Self : in out FSM_Spec;
      S : String)
   is
   begin
      Self.Rejecting_Action := UStrings.To_Unbounded_String (S);
   end Rejecting_Action;

   --  Debug
   procedure Print_Name (Self : FSM_Spec)
   is
      use Ada.Text_IO;
   begin
      Put_Line (UStrings.To_String (Self.Name));
   end Print_Name;

   procedure Print_States (Self : FSM_Spec)
   is
      use Ada.Text_IO;
   begin
      Put_Line ("States:");
      for S in State_Type loop
         Put_Line (S'Image);
      end loop;
   end Print_States;

   procedure Print_Transitions (Self : FSM_Spec)
   is
      use Ada.Text_IO;
      function Transition_Table_Key
         (Position : Transition_Table_Maps.Cursor) return State_Type
         renames Transition_Table_Maps.Key;
      function Transition_Key
         (Position : Transition_Maps.Cursor) return String
         renames Transition_Maps.Key;
   begin
      Put_Line ("Transitions:");
      for S in Self.Transitions.Iterate loop
         Put_Line (Transition_Table_Key (S)'Image);
         for T in Self.Transitions (S).Map.Iterate loop
            Put_Line (Transition_Key (T)
                      & " => "
                      & UStrings.To_String
                           (Self.Transitions (S).Map (T).To_State)
                      & " ("
                      & UStrings.To_String
                        (Self.Transitions (S).Map (T).Action)
                      & ")");
         end loop;
         if Self.Transitions (S).On_EOF.Is_Set then
            Put_Line ("EOF"
                      & " => "
                      & UStrings.To_String
                        (Self.Transitions (S).On_EOF.To_State)
                      & " ("
                      & UStrings.To_String
                        (Self.Transitions (S).On_EOF.Action)
                      & ")");
         end if;
      end loop;
   end Print_Transitions;

end FSM_Generator;
