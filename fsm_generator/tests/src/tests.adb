with FSM_Generator;
with Ada.Text_IO;
with Ada.Assertions;
with My_Test_FSM;
with Ada_Gen;

procedure Tests is
   use Ada.Text_IO;
   use Ada.Assertions;

   procedure Test_Case (S : String;
      Expected_Accepted : Boolean;
      Expected_Count : Natural := 0;
      Expected_Radix_Position : Integer := -1;
      Expected_Last_Leading_Zero : Integer := -1;
      Expected_First_Trailing_Zero : Integer := -1)
   is
      Accepted : Boolean;
      Count : Natural;
      Radix_Position : Integer;
      Last_Leading_Zero : Integer;
      First_Trailing_Zero : Integer;
   begin
      Accepted := My_Test_FSM (S, Count, Radix_Position,
                               Last_Leading_Zero,
                               First_Trailing_Zero);
      New_Line;
      Put_Line (S);

      Put_Line ("Accepted = " & Accepted'Image);
      Assert (Accepted = Expected_Accepted);

      if Expected_Accepted then
         Assert (Count = Expected_Count);
         Assert (Radix_Position = Expected_Radix_Position);
         Assert (Last_Leading_Zero = Expected_Last_Leading_Zero);
         Assert (First_Trailing_Zero = Expected_First_Trailing_Zero);
         Put_Line ("Count = " & Count'Image);
         Put_Line ("Radix_Position = " & Radix_Position'Image);
         Put_Line ("Last_Leading_Zero = " & Last_Leading_Zero'Image);
         Put_Line ("First_Trailing_Zero = " & First_Trailing_Zero'Image);
      end if;
   end Test_Case;

   type State is (Start, Sign, Leading_Zero, Pre_Radix_Digit,
                  Radix_Point, Post_Radix_Digit, Trailing_Zero,
                  Leading_Zero_Separator, Pre_Radix_Digit_Separator,
                  Trailing_Zero_Separator, Post_Radix_Digit_Separator);

   package My_FSM_Generator is new FSM_Generator
      (State_Type => State);
   use My_FSM_Generator;

   FSM : FSM_Spec := Init ("My_Test_FSM", Start);
begin
   if True then
      --  Setup program
      FSM.Setup_Input (Iteration_Mode => By_Index);

      --  Add extra parameters
      FSM.Add_Extra_Parameter ("Count", "Natural", "0");
      FSM.Add_Extra_Parameter ("Radix_Position", "Integer", "-1");
      FSM.Add_Extra_Parameter ("Last_Leading_Zero", "Integer", "-1");
      FSM.Add_Extra_Parameter ("First_Trailing_Zero", "Integer", "-1");

      --  State: Start
      FSM.Add_Transition (Start,
                          "'+' | '-'", Sign);
      FSM.Add_Transition (Start,
                          "'0'", Leading_Zero);
      FSM.Add_Transition (Start,
                          "'1' .. '9'", Pre_Radix_Digit);

      --  State: Sign
      FSM.Add_Transition (Sign,
                          "'0'", Leading_Zero);
      FSM.Add_Transition (Sign,
                          "'1' .. '9'", Pre_Radix_Digit);

      --  State: Leading_Zero
      FSM.Add_State_Entry_Action (Leading_Zero,
         Ada_Gen.Statement_Increment ("Count"), All_States);
      FSM.Add_State_Entry_Action (Leading_Zero,
         Ada_Gen.Statement_Assignment ("Last_Leading_Zero", Input_Index),
         All_States);

      FSM.Add_Transition (Leading_Zero,
                          "'0'", Leading_Zero);
      FSM.Add_Transition (Leading_Zero,
                          "'1' .. '9'", Pre_Radix_Digit);
      FSM.Add_Transition (Leading_Zero,
                          "'_'", Leading_Zero_Separator);
      FSM.Add_Transition (Leading_Zero,
                          "'.'", Radix_Point);
      FSM.Add_Transition (Leading_Zero,
                          EOF, Leading_Zero,
                          Kind => Accepting);

      --  State: Leading_Zero_Separator
      FSM.Add_Transition (Leading_Zero_Separator,
                          "'0'", Leading_Zero);
      FSM.Add_Transition (Leading_Zero_Separator,
                          "'1' .. '9'", Pre_Radix_Digit);

      --  State: Pre_Radix_Digit
      FSM.Add_State_Entry_Action (Pre_Radix_Digit,
         Ada_Gen.Statement_Increment ("Count"), All_States);

      FSM.Add_Transition (Pre_Radix_Digit,
                          "'0' .. '9'", Pre_Radix_Digit);
      FSM.Add_Transition (Pre_Radix_Digit,
                          "'_'", Pre_Radix_Digit_Separator);
      FSM.Add_Transition (Pre_Radix_Digit,
                          "'.'", Radix_Point);
      FSM.Add_Transition (Pre_Radix_Digit,
                          EOF, Pre_Radix_Digit,
                          Kind => Accepting);

      --  State: Pre_Radix_Digit_Separator
      FSM.Add_Transition (Pre_Radix_Digit_Separator,
                          "'0'", Pre_Radix_Digit);
      FSM.Add_Transition (Pre_Radix_Digit_Separator,
                          "'1' .. '9'", Pre_Radix_Digit);

      --  State: Radix_Point
      FSM.Add_State_Entry_Action (Radix_Point,
        Ada_Gen.Statement_If_Single
            (Ada_Gen.Operator_Is_Member ("Radix_Position",
                                         Ada_Gen.Attribute (Input_Buffer,
                                                            "Range")),
             FSM.Rejecting_Action)
         + Ada_Gen.Statement_Assignment ("Radix_Position",
                                         Input_Index));

      FSM.Add_Transition (Radix_Point,
                          "'0'", Trailing_Zero);
      FSM.Add_Transition (Radix_Point,
                          "'1' .. '9'", Post_Radix_Digit);

      --  State: Trailing_Zero
      FSM.Add_State_Entry_Action (Trailing_Zero,
         Ada_Gen.Statement_Increment ("Count"), All_States);
      FSM.Add_State_Entry_Action (Trailing_Zero,
        Ada_Gen.Statement_If_Single
            (Ada_Gen.Operator_Is_Not_Member ("First_Trailing_Zero",
                                             Ada_Gen.Attribute (Input_Buffer,
                                                                "Range")),
             Ada_Gen.Statement_Assignment ("First_Trailing_Zero",
                                           Input_Index)));

      FSM.Add_Transition (Trailing_Zero,
                          "'0'", Trailing_Zero);
      FSM.Add_Transition (Trailing_Zero,
                          "'1' .. '9'", Post_Radix_Digit);
      FSM.Add_Transition (Trailing_Zero,
                          "'_'", Trailing_Zero_Separator);
      FSM.Add_Transition (Trailing_Zero,
                          EOF, Trailing_Zero,
                          Kind => Accepting);

      --  State: Trailing_Zero_Separator
      FSM.Add_Transition (Trailing_Zero_Separator,
                          "'0'", Trailing_Zero);
      FSM.Add_Transition (Trailing_Zero_Separator,
                          "'1' .. '9'", Post_Radix_Digit);

      --  State: Post_Radix_Digit
      FSM.Add_State_Entry_Action (Post_Radix_Digit,
         Ada_Gen.Statement_Increment ("Count"), All_States);
      FSM.Add_State_Entry_Action (Post_Radix_Digit,
         Ada_Gen.Statement_Assignment ("First_Trailing_Zero", "-1"));

      FSM.Add_Transition (Post_Radix_Digit,
                          "'0'", Trailing_Zero);
      FSM.Add_Transition (Post_Radix_Digit,
                          "'1' .. '9'", Post_Radix_Digit);
      FSM.Add_Transition (Post_Radix_Digit,
                          "'_'", Post_Radix_Digit_Separator);
      FSM.Add_Transition (Post_Radix_Digit,
                          EOF, Post_Radix_Digit,
                          Kind => Accepting);

      --  State: Post_Radix_Digit_Separator
      FSM.Add_Transition (Post_Radix_Digit_Separator,
                          "'0'", Post_Radix_Digit);
      FSM.Add_Transition (Post_Radix_Digit_Separator,
                          "'1' .. '9'", Post_Radix_Digit);

      FSM.Generate ("src/my_test_fsm.adb");
   end if;

   --  Pass cases
   if True then
      Test_Case ("10_01.230_00_1", True, 10, 6, -1, -1);
      Test_Case ("0_01.230_00", True, 8, 5, 3, 8);
   end if;

   --  Fail cases
   if True then
      Test_Case ("10_01.230_00__1", False);
      Test_Case ("10__01.230_00_1", False);
      Test_Case ("10_.01230_00_1", False);
      Test_Case ("10_01._230_00_1", False);
   end if;
end Tests;
