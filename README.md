# ada-projects

Home for my Ada projects.

I'm learning how to use the language and toolchain :)

## Projects

+ **matrix_traversal** : find unique symmetric ways to traverse an NxN image.
+ **elementary_arithmetic** : implementation of elementary school algorithms for arithmetic operations
+ **elementary_calculator** : simple calculator using algorithms from elementary school
+ **fsm_generator** : Ada code generator for finite state machines
+ **ada_gen**: Ada code generation framework (work in progress)
