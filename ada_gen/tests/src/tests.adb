with Ada_Gen;
with Ada.Text_IO;

procedure Tests is
   use Ada_Gen;
   use Ada.Text_IO;

   Res1 : constant String := Statement_Increment ("MyVar");
   Res2 : constant String := Statement_Decrement ("MyVar");
   Res3 : constant String := Statement_Exit ("i = 0", "MyLoop");
begin
   Put_Line (Res1);
   Put_Line (Res2);
   Put_Line (Res3);
end Tests;
