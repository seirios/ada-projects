package Ada_Gen is

   Tab : constant String := "   ";

   -- Utility functions
   function Parenthesize (Contents : String)
      return String
   is ("(" & Contents & ")");

   -- 4. Names and Expressions

   --- 4.1 Names

   ---- 4.1.4 Attributes
   function Attribute (Prefix : String; AttrName : String)
      return String
   is (Prefix & "'" & AttrName);

   --- 4.5 Operators and Expression Evaluation
   function Operator_Unary (Symbol : String; Right : String;
                            Use_Parentheses : Boolean := False)
      return String
   is (if Use_Parentheses then Parenthesize (Symbol & " " & Parenthesize (Right))
                          else (Symbol & " " & Right));

   function Operator_Binary (Symbol : String; Left : String; Right : String;
                             Use_Parentheses : Boolean := False)
      return String
   is (if Use_Parentheses then Parenthesize (Parenthesize (Left) & " " & Symbol
                                             & " " & Parenthesize (Right))
                          else (Left & " " & Symbol & " " & Right));

   ---- 4.5.1 Logical Operators and Short-circuit Control Forms
   function Operator_Logical_And (Left : String; Right : String;
                                  Use_Parentheses : Boolean := False)
      return String
   is (Operator_Binary ("and", Left, Right, Use_Parentheses));

   function Operator_Logical_Or (Left : String; Right : String;
                                 Use_Parentheses : Boolean := False)
      return String
   is (Operator_Binary ("or", Left, Right, Use_Parentheses));

   function Operator_Logical_Xor (Left : String; Right : String;
                                  Use_Parentheses : Boolean := False)
      return String
   is (Operator_Binary ("xor", Left, Right, Use_Parentheses));

   function Operator_Short_Circuit_And (Left : String; Right : String;
                                        Use_Parentheses : Boolean := False)
      return String
   is (Operator_Binary ("and then", Left, Right, Use_Parentheses));

   function Operator_Short_Circuit_Or (Left : String; Right : String;
                                       Use_Parentheses : Boolean := False)
      return String
   is (Operator_Binary ("or else", Left, Right, Use_Parentheses));

   ---- 4.5.2 Relational Operators and Membership Tests
   function Operator_Equals (Left : String; Right : String;
                             Use_Parentheses : Boolean := False)
      return String
   is (Operator_Binary ("=", Left, Right, Use_Parentheses));

   function Operator_Not_Equals (Left : String; Right : String;
                                 Use_Parentheses : Boolean := False)
      return String
   is (Operator_Binary ("/=", Left, Right, Use_Parentheses));

   function Operator_Less_Than (Left : String; Right : String;
                                Use_Parentheses : Boolean := False)
      return String
   is (Operator_Binary ("<", Left, Right, Use_Parentheses));

   function Operator_Less_Than_Or_Equal (Left : String; Right : String;
                                         Use_Parentheses : Boolean := False)
      return String
   is (Operator_Binary ("<=", Left, Right, Use_Parentheses));

   function Operator_Greater_Than (Left : String; Right : String;
                                   Use_Parentheses : Boolean := False)
      return String
   is (Operator_Binary (">", Left, Right, Use_Parentheses));

   function Operator_Greater_Than_Or_Equal (Left : String; Right : String;
                                            Use_Parentheses : Boolean := False)
      return String
   is (Operator_Binary (">=", Left, Right, Use_Parentheses));

   function Operator_Is_Member (Left : String; Right : String;
                                Use_Parentheses : Boolean := False)
      return String
   is (Operator_Binary ("in", Left, Right, Use_Parentheses));

   function Operator_Is_Not_Member (Left : String; Right : String;
                                    Use_Parentheses : Boolean := False)
      return String
   is (Operator_Binary ("not in", Left, Right, Use_Parentheses));

   ---- 4.5.3 Binary Adding Operators
   function Operator_Addition (Left : String; Right : String;
                               Use_Parentheses : Boolean := False)
      return String
   is (Operator_Binary ("+", Left, Right, Use_Parentheses));

   function Operator_Subtraction (Left : String; Right : String;
                                  Use_Parentheses : Boolean := False)
      return String
   is (Operator_Binary ("-", Left, Right, Use_Parentheses));

   function Operator_Concatenation (Left : String; Right : String;
                                    Use_Parentheses : Boolean := False)
      return String
   is (Operator_Binary ("&", Left, Right, Use_Parentheses));

   ---- 4.5.4 Unary Adding Operators
   function Operator_Identity (Right : String;
                               Use_Parentheses : Boolean := False)
      return String
   is (Operator_Unary("+", Right, Use_Parentheses));

   function Operator_Negation (Right : String;
                               Use_Parentheses : Boolean := False)
      return String
   is (Operator_Unary("-", Right, Use_Parentheses));

   ---- 4.5.5 Multiplying operators
   function Operator_Multiplication (Left : String; Right : String;
                                     Use_Parentheses : Boolean := False)
      return String
   is (Operator_Binary ("*", Left, Right, Use_Parentheses));

   function Operator_Division (Left : String; Right : String;
                               Use_Parentheses : Boolean := False)
      return String
   is (Operator_Binary ("/", Left, Right, Use_Parentheses));

   function Operator_Modulus (Left : String; Right : String;
                              Use_Parentheses : Boolean := False)
      return String
   is (Operator_Binary ("mod", Left, Right, Use_Parentheses));

   function Operator_Remainder (Left : String; Right : String;
                                Use_Parentheses : Boolean := False)
      return String
   is (Operator_Binary ("rem", Left, Right, Use_Parentheses));

   ---- 4.5.6 Highest Precedence Operators
   function Operator_Logical_Not (Right : String;
                                  Use_Parentheses : Boolean := False)
      return String
   is (Operator_Unary("not", Right, Use_Parentheses));

   function Operator_Absolute_Value (Right : String;
                                     Use_Parentheses : Boolean := False)
      return String
   is (Operator_Unary("abs", Right, Use_Parentheses));

   function Operator_Exponentiation (Left : String; Right : String;
                                     Use_Parentheses : Boolean := False)
      return String
   is (Operator_Binary ("**", Left, Right, Use_Parentheses));

   -- 5. Statements

   --- 5.1 Simple and Compound Statements

   --- FIXME: should accept a list of labels
   function Statement_Simple (Contents : String; Label : String := "")
      return String
   is ((if Label /= "" then ("<<" & Label & ">> ") else "") & Contents & ";");

   function Statement_Null
      return String
   is (Statement_Simple ("null"));

   --- 5.2 Assignment Statements 
   function Statement_Assignment (VarName : String; Expr : String)
      return String
   is (Statement_Simple (VarName & " := " & Expr));

   function Statement_Increment (VarName : String; Step : String := "1")
      return String
   is (Statement_Assignment (VarName, Operator_Addition(VarName, Step)));

   function Statement_Decrement (VarName : String; Step : String := "1")
      return String
   is (Statement_Assignment (VarName, Operator_Subtraction(VarName, Step)));

   --- 5.3 If Statements
   --- TODO: multi-statement, multi-branch
   function Statement_If_Single (Condition : String; Statement : String;
                                 Statement_Else : String := "")
      return String
   is ("if " & Condition & " then" & ASCII.LF
       & Tab & Statement & ASCII.LF
       & (if Statement_Else /= "" then ("else" & ASCII.LF
                                        & Tab & Statement_Else & ASCII.LF)
                                  else "")
       & "end if;");

   --- 5.7 Exit Statements
   function Statement_Exit (Condition : String := ""; LoopName : String := "")
      return String
   is (Statement_Simple ("exit"
                         & (if LoopName /= "" then (" " & LoopName) else "")
                         & (if Condition /= "" then (" when " & Condition) else "")));

   --- 5.8 Goto Statements 
   function Statement_Goto (LabelName : String)
      return String
   is (Statement_Simple ("goto " & LabelName));

   -- 6. Subprograms 

   --- 6.5 Return Statements
   function Statement_Return_Simple (Expr : String)
      return String
   is (Statement_Simple ("return " & Expr));

end Ada_Gen;
